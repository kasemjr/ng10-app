import { Component } from '@angular/core';
import { render } from '@testing-library/angular';
import { UiLoadingModule } from './ui-loading.module';
import { UiLoadingService } from './ui-loading.service';

@Component({
  template: `<ui-loading></ui-loading>`
})
class TestWrapperComponent {}

describe('UiLoadingComponent', () => {
  it('should be created', async () => {
    const { container } = await render(TestWrapperComponent, {
      imports: [
        UiLoadingModule.forRoot({
          defaultAnimationPath: ''
        })
      ]
    });

    expect(container.querySelector('.ui-loading')).toBeDefined();
  });

  it('should be not displayed after creation', async () => {
    const { container } = await render(TestWrapperComponent, {
      imports: [
        UiLoadingModule.forRoot({
          defaultAnimationPath: ''
        })
      ]
    });

    expect(container.querySelector('.ui-loading')).toBeDefined();
    expect(container.querySelector('.show-loading')).toBeNull();
  });

  it('should displays the loading component', async () => {
    const loadingService = new UiLoadingService();

    const { container } = await render(TestWrapperComponent, {
      imports: [
        UiLoadingModule.forRoot({
          defaultAnimationPath: ''
        })
      ],
      providers: [
        {
          provide: UiLoadingService,
          useValue: loadingService
        }
      ]
    });

    loadingService.show();

    expect(container.querySelector('.ui-loading')).toBeDefined();
    expect(container.querySelector('.show-loading')).toBeDefined();
  });

  it('should hides the loading component if it is visible', async () => {
    const loadingService = new UiLoadingService();

    const { container } = await render(TestWrapperComponent, {
      imports: [
        UiLoadingModule.forRoot({
          defaultAnimationPath: ''
        })
      ],
      providers: [
        {
          provide: UiLoadingService,
          useValue: loadingService
        }
      ]
    });

    loadingService.show();

    expect(container.querySelector('.ui-loading')).toBeDefined();
    expect(container.querySelector('.show-loading')).toBeDefined();

    loadingService.hide();

    expect(container.querySelector('.show-loading')).toBeNull();
  });
});
