import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';
import { UiLoadingConfig } from './ui-loading.config';
import { UiLoadingService } from './ui-loading.service';

@Component({
  selector: 'ui-loading',
  templateUrl: `./ui-loading.component.html`,
  styleUrls: ['./ui-loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class UiLoadingComponent {
  isVisible$ = this.service.isVisible$;

  options: AnimationOptions = {
    path: this.config.defaultAnimationPath
  };

  constructor(private config: UiLoadingConfig, private service: UiLoadingService) {}
}
