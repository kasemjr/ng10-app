import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-button',
  template: `<button type="button" (click)="clickEvent.emit($event)" [disabled]="!enabled">
    <ng-content></ng-content>
  </button>`,
  styles: []
})
export class UiButtonComponent {
  @Input()
  enabled = true;

  @Output()
  clickEvent: EventEmitter<Event> = new EventEmitter<Event>();
}
