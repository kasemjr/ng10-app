import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-form-label',
  templateUrl: './ui-form-label.component.html',
  styleUrls: ['./ui-form-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UiFormLabelComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
