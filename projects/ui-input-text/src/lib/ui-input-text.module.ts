import { NgModule } from '@angular/core';
import { UiInputTextComponent } from './ui-input-text.component';

@NgModule({
  declarations: [UiInputTextComponent],
  imports: [],
  exports: [UiInputTextComponent]
})
export class UiInputTextModule {}
