import { TestBed } from '@angular/core/testing';

import { UiInputTextService } from './ui-input-text.service';

describe('UiInputTextService', () => {
  let service: UiInputTextService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UiInputTextService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
