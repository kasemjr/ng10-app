import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { UiButtonModule } from 'ui-button';
import { UiFormFieldModule } from 'ui-form-field';
import { UiInputTextModule } from 'ui-input-text';
import { FormsRoutingModule } from './forms-routing.module';
import { FormsComponent } from './forms.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsRoutingModule,
    UiFormFieldModule,
    UiInputTextModule,
    UiButtonModule
  ],
  declarations: [FormsComponent]
})
export class FormsModule {}
