const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const mf = require('@angular-architects/module-federation/webpack');
const path = require('path');

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(path.join(__dirname, '../../tsconfig.json'), [
  /* mapped paths to share */
]);

module.exports = {
  output: {
    uniqueName: 'app_forms',
    publicPath: 'auto'
  },
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases()
    }
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'app_forms',
      filename: 'remoteEntry.js',
      exposes: {
        './Module': './projects/app-forms/src/app/forms/forms.module.ts'
      },
      shared: {
        '@angular/core': { singleton: true, strictVersion: true, requiredVersion: '^12.0.0' },
        '@angular/common': { singleton: true, strictVersion: true, requiredVersion: '^12.0.0' },
        '@angular/router': { singleton: true, strictVersion: true, requiredVersion: '^12.0.0' },
        ...sharedMappings.getDescriptors()
      }
    }),
    sharedMappings.getPlugin()
  ]
};
