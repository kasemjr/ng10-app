import { loadRemoteModule } from '@angular-architects/module-federation';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'dummy', loadChildren: () => import('./dummy/dummy.module').then((m) => m.DummyModule) },
  {
    path: 'forms',
    loadChildren: () =>
      loadRemoteModule({
        remoteEntry: 'http://localhost:4230/remoteEntry.js',
        remoteName: 'app_forms',
        exposedModule: './Module'
      }).then((m) => m.FormsModule)
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
