import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { UiButtonModule } from 'projects/ui-button/src/public-api';
import { UiModalModule } from 'projects/ui-modal/src/public-api';
import { UiLoadingModule } from 'ui-loading';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiButtonModule,
    UiModalModule,
    UiLoadingModule.forRoot({ defaultAnimationPath: '/assets/animations/dirigible.json' })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
